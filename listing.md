# Kill Team Roster

## Base

	* Forward Base (250pts Team Limit)
	* Comms Relay - Reroll reserves.

## Roster

### Terminator Champion `Leader` - 69pts

*Mordenon*

##### Stats

WS |BS | S |  T  | W | I | A |Ld |Sv 
:-:|:-:|:-:|:---:|:-:|:-:|:-:|:-:|:-:
 4 | 4 | 4 |4 (5)| 2 | 4 | 2 | 9 |2+

##### Special Rules

  * Inspiring Presence
  * Champion of Chaos
  * Champion of Traitors
  * Mark of Nurgle:
    * +1 Toughness
  * Terminator Armour:
    * 5+ Invulnerable Save
    * Bulky
    * Deep Strike
    * Relentless
    * May not make Sweeping Advances
  * Daemonix Essence:
    * Feel No Pain (5+)

##### War Gear

  * Combi-Bolter
  * Power Sword
  * Terminator Armour
	* Chaos Hound
	* Teleporter

### Chaos Terminator `Core` - 37pts

*Anufar*

##### Stats

WS |BS | S |  T  | W | I | A |Ld |Sv 
:-:|:-:|:-:|:---:|:-:|:-:|:-:|:-:|:-:
 4 | 4 | 4 |4 (5)| 1 | 4 | 2 | 9 |2+

##### Special Rules

  * Mark of Nurgle:
    * +1 Toughness
  * Terminator Armour:
    * 5+ Invulnerable Save
    * Bulky
    * Deep Strike
    * Relentless
    * May not make Sweeping Advances

##### War Gear

  * Combi-Bolter
  * Power Axe
  * Terminator Armour

### Chaos Terminator `Core` - 37pts

*Talomort*

##### Stats

WS |BS | S |  T  | W | I | A |Ld |Sv 
:-:|:-:|:-:|:---:|:-:|:-:|:-:|:-:|:-:
 4 | 4 | 4 |4 (5)| 1 | 4 | 2 | 9 |2+

##### Special Rules

  * Mark of Nurgle:
    * +1 Toughness
  * Terminator Armour:
    * 5+ Invulnerable Save
    * Bulky
    * Deep Strike
    * Relentless
    * May not make Sweeping Advances

##### War Gear

  * Combi-Bolter
  * Power Sword
  * Terminator Armour

### Chaos Terminator `Special` - 54pts

*Guraruman*

##### Stats

WS |BS | S |  T  | W | I | A |Ld |Sv 
:-:|:-:|:-:|:---:|:-:|:-:|:-:|:-:|:-:
 4 | 4 | 4 |4 (5)| 1 | 4 | 2 | 9 |2+

##### Special Rules

  * Mark of Nurgle:
    * +1 Toughness
  * Terminator Armour:
    * 5+ Invulnerable Save
    * Bulky
    * Deep Strike
    * Relentless
    * May not make Sweeping Advances

##### War Gear

  * Combi-Bolter
  * Power Fist
  * Terminator Armour
	* Icon of Despair

### Chaos Terminator `Special` - 49pts

*Yganath*

##### Stats

WS |BS | S |  T  | W | I | A |Ld |Sv 
:-----:|:-:|:-:|:---:|:-:|:-:|:-:|:-:|:-:
 4 (3) | 4 | 4 |4 (5)| 1 | 4 | 2 | 9 |2+

##### Special Rules

  * Mark of Nurgle:
    * +1 Toughness
  * Terminator Armour:
    * 5+ Invulnerable Save
    * Bulky
    * Deep Strike
    * Relentless
    * May not make Sweeping Advances

##### War Gear

  * Lightning Claws
  * Terminator Armour
	* Teleporter

##### Injury

  * Maimed, -1WS
	
### Chaos Terminator `Special` - 49pts

*Kybire*

##### Stats

WS |BS | S |  T  | W | I | A |Ld |Sv 
:-:|:-:|:-:|:---:|:-:|:-:|:-:|:-:|:-:
 4 | 4 | 4 |4 (5)| 1 | 4 | 2 | 9 |2+

##### Special Rules

  * Mark of Nurgle:
    * +1 Toughness
  * Terminator Armour:
    * 5+ Invulnerable Save
    * Bulky
    * Deep Strike
    * Relentless
    * May not make Sweeping Advances

##### War Gear

  * Lightning Claws
  * Terminator Armour
	* Teleporter
	
### Chosen `Special` - 31pts

*Rhyroz*

##### Stats

WS |BS | S |  T  | W | I | A |Ld |Sv 
:-:|:-:|:-:|:---:|:-:|:-:|:-:|:-:|:-:
 4 | 4 | 4 |4 (5)| 1 | 4 | 2 | 9 |3+

##### Special Rules

  * Mark of Nurgle:
    * +1 Toughness

##### War Gear

  * Power Armour
	* Melta Gun
	* Bolt Pistol
	* Close Combat Weapon
	* Frag Grenade
	* Krak Grenade

### Cultists `Core`

*Kudred, Runacus, Ruram, Savarak, Pargemor*

##### Stats

WS |BS | S | T | W | I | A |Ld |Sv 
:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:
 3 | 3 | 3 | 3 | 1 | 3 | 1 | 7 | 6+

##### Special Rules

  * Squad (5)

##### War Gear

  * Improvised Armour
  * Autopistol
  * Close Combat Weapon

### Plague Zombies `Core`

*Foo, Bar, Baz, Qux, Quux.*

##### Stats

WS |BS | S | T | W | I | A |Ld |Sv 
:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:
 3 | 3 | 3 | 3 | 1 | 3 | 1 | 7 |6+

##### Special Rules

  * Squad (5)
  * Fearless
  * Feel No Pain
  * Slow and Purposefu

##### War Gear

  * Close Combat Weapon

### Plague Zombies `Core`

*Corge, Grault, Garply, Waldo, Plugh.*

##### Stats

WS |BS | S | T | W | I | A |Ld |Sv 
:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:
 3 | 3 | 3 | 3 | 1 | 3 | 1 | 7 |6+

##### Special Rules

  * Squad (5)
  * Fearless
  * Feel No Pain
  * Slow and Purposefu

##### War Gear

  * Close Combat Weapon

