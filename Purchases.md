# Kill Team Initial Initial Force Roster

#### Chaos Space Marines

#### Traitor Legions

`Your Kill Team may belong to one of the following Traitor Legions, in which case, unless otherwise stated within the rule, all Chaos Acolyte, Aspiring Champion, Chaos Space Marine, Chaos Terminators, Chosen, Raptor and Chaos Biker models in your Kill Team gain the special rules and effects as indicated below for free.`

##### Death Guard

`Models have the Feel No Pain (6+) special rule. If they have the Feel No Pain rule from another source, they instead add 1 to the result of any Feel No Pain rolls. Cultists may become Plague Zombies as in the Chaos Space Marine codex. The army cannot include Marks of Tzeentch, Icons of Flame or Thousand Sons marines.`

## Leader

  * Terminator Champion - *33pt (Sub Total: 54pts)*
    * Mark of Nurgle (Opt.) - *6pts*
    * Daemonic Essence (W.G.)  - *15pts*

Sub Total: 54pts

## Core

  * Terminator - *31pts (Sub Total: 37pts)*
    * Mark of Nurgle (Opt.) - *6pts*
  * Cultists - *20pts*
  * Plague Zombies - *20pts*
    * Traitor Legions - Death Guard: `"...Cultists may become Plague Zombies as in the Chaos Space Marine codex..."`

Sub Total: 77pts

## Special

  * Terminator - *31pts (Sub Total: 44pts)*
    * Mark of Nurgle (Opt.) - *6pts*
    * Power Claws (W.G.) - *7pts*
  * Terminator - *31pts (Sub Total: 44pts)*
    * Mark of Nurgle (Opt.) - *6pts*
    * Power Fist (W.G.) - *7pts*
  *  Chosen - *18pts (Sub Total: 31)*
    * Mark of Nurgle (Opt.) - *3pts*
    * Melta Gun (Opt.) - *10pts*

Sub Total: 119pts

Roster Total: 250pts

---

# Game 1 - Win

*vs. Scions (Paul)*

  * +2ren
  * +18rp
    * +3rp Mission Objective
    * +10rp Participation
    * +5rp Win
  * Anufar Killed (Terminator w/Power Axe & Combi-bolter)

## Upgrades

  * -15rp (75pts, using 72pts)
    * Chaos Hound (W.G.) - *10pts*
    * Teleporter (W.G) - *5pts*
    * Plague Zombies - *20pts*
    * Terminator - *31pts (Sub Total: 37pts)*
      * Mark of Nurgle (Opt.) - *6pts*

---

# Game 2 - Loss

*vs. Inquisitors (Luke)*

  * -1ren
  * +10rp
    * +10rp Participation
  * Guraruman Winded (Terminator w/Powerfist)

---

# Game 3 - Loss

*vs. Assasins (Sam)*

  * -1ren
  * +10rp
    * +10rp Participation

## Upgrades

  * -1rp (5pts, using 5pts)
    * Teleporter (W.G) - *5pts*
		
---

# Totals

RP: 21
Ren: 0

---

## Leader

  * Terminator Champion - *33pt (Sub Total: 69pts)*
    * Mark of Nurgle (Opt.) - *6pts*
    * Daemonic Essence (W.G.)  - *15pts*
    * Chaos Hound (W.G.) - *10pts*
		* Teleporter (W.G.) - *5pts*

Sub Total: 69pts

## Core

  * Terminator - *31pts (Sub Total: 37pts)*
    * Mark of Nurgle (Opt.) - *6pts*
  * Cultists - *20pts*
  * Plague Zombies - *20pts*
  * Plague Zombies - *20pts*
    * Traitor Legions - Death Guard: `"...Cultists may become Plague Zombies as in the Chaos Space Marine codex..."`

Sub Total: 97pts

## Special

  * Terminator - *31pts (Sub Total: 49pts)*
    * Mark of Nurgle (Opt.) - *6pts*
    * Power Claws (W.G.) - *7pts*
    * Teleporter (W.G.) - *5pts*
  * Terminator - *31pts (Sub Total: 44pts)*
    * Mark of Nurgle (Opt.) - *6pts*
    * Power Fist (W.G.) - *7pts*
  *  Chosen - *18pts (Sub Total: 31)*
    * Mark of Nurgle (Opt.) - *3pts*
    * Melta Gun (Opt.) - *10pts*

Sub Total: 124pts

Roster Total: 290pts

---

# Game 4 - Draw

*vs. Tyranids (Andy)*

  * +1ren
  * +15rp
    * +10rp Participation
		* +3rp Stalemate
    * +2rp First Blood
  * +2 Scrap

---

# Game 5 - Loss

*vs. Scions (Paul)*

  * -1ren
  * +14rp
    * +10rp Participation
    * +2rp First Blood
    * +5rp Relics

---

# Totals

RP: 50
Ren: 0
Scrap: 2

---

# Game 6 - Loss

*vs. Tau (Ned)*

  * -1ren (0 total)
  * +10rp
    * +10rp Participation
  * Guraruman Winded (Terminator w/Powerfist)
  * Anufar Dead (Terminator w/Axe)
  * Yganath Dead (Terminator w/Claws)

## Upgrades

  * -18rp (90pts, using 86pts)
    * Terminator - *31pts (Sub Total: 37pts)* (replace Anufar)
      * Mark of Nurgle (Opt.) - *6pts*
    * Terminator - *31pts (Sub Total: 49pts)* (replace Yganath)
      * Mark of Nurgle (Opt.) - *6pts*
      * Power Claws (W.G.) - *7pts*
      * Teleporter (W.G.) - *5pts*
	* Comms Relay - *5rp, 1 scrap*
	
---

# Totals

RP: 37
Ren: 0
Scrap: 1

---

# Game 7 - Loss

*vs. Deathwatch (Dan)*

  * +1ren
  * +10rp
    * +10rp Participation
  * Guraruman Incapacitated (Terminator w/Powerfist)

---

# Game 8 - Loss

*vs. Assasins (Sam)*

  * +1ren
  * +12rp
    * +10rp Participation
    * +2rp Underdog
  * Mordenon dead (Terminator Leader)
	* Foo Dead (Zombie)
	* Kudred Dead (Cultist)
  * Yganath -1WS (Terminator w/Claws)

## Upgrades

  * -26rp (130pts, using 128pts)
    * Terminator - *31pts (Sub Total: 49pts)* (Sort of replacing Yganath (-1WS), with Kybire)
      * Mark of Nurgle (Opt.) - *6pts*
      * Power Claws (W.G.) - *7pts*
      * Teleporter (W.G.) - *5pts*
    * Terminator Champion - *33pt (Sub Total: 69pts)*
      * Mark of Nurgle (Opt.) - *6pts*
      * Daemonic Essence (W.G.)  - *15pts*
      * Chaos Hound (W.G.) - *10pts*
  		* Teleporter (W.G.) - *5pts*
		* 1x Cultist - *5pts*
		* 1x Plague Zombie - *5pts*
	* Forward Base - *5rp, 1 scrap*

---

# Totals

RP: 28
Ren: 2
Scrap: 0

---

# Game 9 - Draw

*vs. Astra Militarum (Mike)*

  * +1ren
  * +14rp
    * +10rp Participation
    * +3rp Draw
		* +1rp Special Kill
	* Foo Dead (Zombie)

## Upgrades

  * 11rp (55ts, using 52pts)
		* 1x Plague Zombie - *5pts*
		* Terminator - *31pts (Sub Total: 37pts)*
			* Mark of Nurgle (Opt.) - *6pts*
		* Icon of Despair (Opt.) - *10pts*
		
---

# Totals

RP: 28
Ren: 3
Scrap: 0

---

# Game 10 - Win

*vs. Astra Militarum (Mike)*

  * +2ren
  * +18ptsrp
    * +10rp Participation
    * +5rp Win
		* +3rp Relics

## Upgrades

  * 11rp (55ts, using 52pts)
		* 1x Plague Zombie - *5pts*
		* Terminator - *31pts (Sub Total: 37pts)*
			* Mark of Nurgle (Opt.) - *6pts*
		* Icon of Despair (Opt.) - *10pts*
		
---

# Totals

RP: 46
Ren: 5
Scrap: 0

---